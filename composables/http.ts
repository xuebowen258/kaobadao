// import { useFetch, UseFetchOptions } from "#app";

// function isArray(str: unknown) {
//   return Object.prototype.toString.call(str) === '[object Array]'
// }

// //全局基础URL
// const BASEURL: string = "http://8.130.120.131:9999";  //全局后台服务器请求地址

// //定义ts变量类型接口
// interface HttpParms {
//   baseURL?: string, //请求的基本URL，即后台服务器地址
//   url: string, //请求api接口地址
//   method?: any, //请求方法
//   query?: any, //添加查询搜索参数到URL
//   body?: any //请求体
// }

// /**
//  * 网络请求方法
//  * @param obj 请求参数
//  * @returns 响应结果
//  */
// export const http = (obj: HttpParms) => {
//   const res = new Promise<void>((resolve, reject) => {
//     useFetch(
//       (obj.baseURL ?? BASEURL) + obj.url,
//       {
//         method: obj.method ?? "GET",
//         query: obj?.query ?? null,
//         body: obj?.body ?? null,
//         onRequest({ request, options }) {
//           let token = useToken().value
//           // 设置请求报头
//           options.headers = (options.headers || {}) as { [key: string]: string }
//           options.headers.Authorization = token ? 'Bearer ' + token : 'Basic a2JkYW8tcGM6MTIzNDU2'
//         },
//         onRequestError({ request, options, error }) {
//           // 处理请求错误
//           console.log("服务器链接失败!")
//           reject(error)
//         },
//         onResponse({ request, response, options }) {
//           // 处理响应数据
//           if (+response.status === 200 && +response._data.code !== 200) {
//             resolve(response._data)
//           } else {
//             ElMessage.error(isArray(response._data.msg) ? response._data.msg[0] : response._data.msg)
//           }
//         },
//         onResponseError({ request, response, options }) {
//           // 处理响应错误
//           ElMessage.error(isArray(response._data.msg) ? response._data.msg[0] : response._data.msg)
//         }
//       }
//     )
//   })
//   return res;
// }

import { useFetch, UseFetchOptions } from "#app";

function isArray(str: unknown) {
  return Object.prototype.toString.call(str) === '[object Array]'
}

//全局基础URL
const BASEURL: string = "http://8.130.120.131:9999";

export const http = <T= unknown>(url: string, opts: UseFetchOptions<T, unknown>={}) => {
  // const runtimeConfig = useRuntimeConfig()

  const defaultOptions: UseFetchOptions<unknown> = {
    // baseURL: runtimeConfig.public.baseUrl,
    baseURL: BASEURL,
    method: opts.method ?? "GET",
    query: opts?.query ?? null,
    body: opts?.body ?? null,
    onRequest({ options }) {
      let token = useToken().value
      // 设置请求报头
      options.headers = (options.headers || {}) as { [key: string]: string }
      options.headers.Authorization = token ? 'Bearer ' + token : 'Basic a2JkYW8tcGM6MTIzNDU2'
    },
    onResponse({ response }) {
      if (+response.status === 200 && +response._data.code !== 200) {
        
      } else {
        process.client && ElMessage.error(response._data.msg || '服务器错误')
      }
    },
    onResponseError({ response }) {
      process.client && ElMessage.error(response._data.msg || '服务器错误')
    },
  }

  return useFetch<T>( url, {...defaultOptions, ...opts} as any)
}