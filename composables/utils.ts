let lens = 0

// 返回数组
export const removeArrs = (arrs: any, i: number) => {
    arrs.splice(i, 1)
    return arrs
}

// 合并时间段
export const joinPeriod = (arrs: any, indexes: number): any => {
    let narr = JSON.parse(JSON.stringify(arrs))
    if (arrs.length != 1) {
        let lasts = arrs[indexes]
        let newarr = JSON.parse(JSON.stringify(removeArrs(narr, indexes)))
        let num = 0
        for (let i=0;i<newarr.length;i++) {
            num ++
            if (num == newarr.length) {
                if (lens != 0) {
                    lens = lens - 1
                    return joinPeriod(arrs, lens)
                } else {
                    return arrs
                }
            }
            if (lasts[0] >= newarr[i][0] && lasts[0] <= newarr[i][1]) {
                if (newarr[i][1] < lasts[1]) {
                    newarr[i][1] = lasts[1]
                }
                return joinPeriod(newarr, i)
            }
        }
    } else {
        return arrs
    }
}

// export const videoPlan = (arrs: any) => {
//     lens = arrs.length - 1
//     let newArr = JSON.parse(JSON.stringify(arrs))
//     let jsonArr = joinPeriod(newArr, lens)
//     console.log(jsonArr)
// }