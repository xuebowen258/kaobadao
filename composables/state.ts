// 公共信息
export const useCommonInfo = () => useState<object>('commonInfo', () => ({}))
// 个人信息
export const useUserInfo = () => useState<object>('userInfo', () => ({}))
// token
export const useToken = () => useState<string>('token', () => {
    const token = useCookie('token')
    return token.value ? token.value : ''
})