// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@element-plus/nuxt'],
  css: [
    '@/assets/css/main.css',
    'element-plus/dist/index.css',
    'element-plus/theme-chalk/display.css'
  ],
  devServer: {
    "host": "0.0.0.0",
    "port": "8090"
  }
})
