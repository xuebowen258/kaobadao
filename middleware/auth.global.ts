export default defineNuxtRouteMiddleware(async (to, from) => {
    const token = useToken()
    const info = useUserInfo()
    const info1 = useCommonInfo()

    // if (!route.matched.length) {
    // redirect('/404')
    // }
    
    if (to.path !== '/login') {
        // 验证登录
        if (!token.value) {
            console.log('未登录')
            abortNavigation()
            return navigateTo('/login')
        }
        // 获取个人信息 判断是否存在个人信息
        if (JSON.stringify(useUserInfo().value) == '{}') {
            http('/kbdao-ums/app-api/v1/members/me', {
                method: 'GET'
            }).then(res => {
                let data = res.data.value
                info.value = {
                    id: data.data.id,
                    openId: data.data.openId,
                    mobile: data.data.mobile,
                    avatar: data.data.avatarUrl,
                    nickname: data.data.nickName
                }
            })
        }
        if (JSON.stringify(useCommonInfo().value) == '{}') {
            const { data: cinfo } = await http('/kbdao-system/api/v1/system/app/info', {
                method: 'GET'
            })
            info1.value = {
                agreementList: cinfo.value.data.agreementList,
                contact: cinfo.value.data.contact,
                copyright: cinfo.value.data.copyright,
                androidCode: cinfo.value.data.androidCode
            }
        }
        
    }

    


})